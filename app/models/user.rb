# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  username   :string(255)
#

class User < ActiveRecord::Base
  # validates_presence_of :username
  validates :username, presence: true, uniqueness: true
  
  has_many :contacts
  has_many :contact_shares
  has_many(
    :shared_contacts,
    through: :contact_shares,
    source: :contact
  )
  
end
