Rails.application.routes.draw do
  
  resources :users, except: [:new, :edit] do
    resources :contacts, only: [:index]
  end
  
  resources :contacts, except: [:index, :new, :edit]
  
  resources :contact_shares, only: [:create, :destroy]
  
  # resources :users
#   get '/users' => 'users#index'
#   post '/users' => 'users#create'
#   get '/users/new' => 'users#new'
#   get '/users/:id/edit' => 'users#edit'
#   get '/users/:id' => 'users#show'
#   patch '/users/:id' => 'users#update'
#   put '/users/:id' => 'users#update'
#   delete '/users/:id' => 'users#destroy'
#   get '/users/:user_id/contacts' => 'users#index'
  
  # resources :users
  # get '/contacts' => 'contacts#index'
#   post '/contacts' => 'contacts#create'
  # get '/contacts/new' => 'contacts#new'
  # get '/contacts/:id/edit' => 'contacts#edit'
  # get '/contacts/:id' => 'contacts#show'
  # patch '/contacts/:id' => 'contacts#update'
  # put '/contacts/:id' => 'contacts#update'
  # delete '/contacts/:id' => 'contacts#destroy'
  
  # post '/contact_shares' => 'contact_shares#create'
  # delete '/contact_shares/:id' => 'contact_shares#destroy'
end
