class ContactShares < ActiveRecord::Migration
  def change
    create_table :contact_shares do |t|
      t.integer :contact_id, :null => false, :unique => true
      t.integer :user_id, :null => false, :unique => true
      
      t.timestamps
    end
    add_index :contact_shares, [:user_id, :contact_id], unique: true
  end
end
